import datetime
import os

from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.urls import reverse
import django_filters

from . import gpxcalc


class Route(models.Model):
    """Cet objet représente une route de randonnée."""
    class Meta:
        permissions = [
            ("show_fiche", "Can download fiche"),
        ]

    nom = models.CharField(max_length=50, unique=True, default='From gpx')
    gpx = models.FileField()
    lat = models.FloatField(default=0, editable=False)
    lon = models.FloatField(default=0, editable=False)
    distance = models.FloatField(default=0, editable=False)
    positif = models.PositiveIntegerField(verbose_name="Dénivelé positif",
                                          default=0, editable=False)
    negatif = models.PositiveIntegerField(verbose_name="Dénivelé négatif",
                                          default=0, editable=False)
    cotation = models.CharField(max_length=5, blank=True)
    notes = models.TextField(blank=True)
    geom = models.TextField(default=None, blank=True, null=True, editable=False)
    altimin = models.PositiveIntegerField(verbose_name="Altitude min", default=0, editable=False)
    altimax = models.PositiveIntegerField(verbose_name="Altitude max", default=0, editable=False)
    estimationtemps = models.DurationField(verbose_name="Temps estimé",
                                           default=datetime.timedelta(minutes=0), editable=False)
    fiche = models.FileField(default=None, blank=True, null=True)

    def __str__(self):
        """Fonction requise par Django pour manipuler les objets Route dans la base de données."""
        return self.nom

    def get_absolute_url(self):
        """Cette fonction est requise par Django, lorsque vous souhaitez détailler le contenu d'un objet."""
        return reverse('route-detail', args=[str(self.id)])

    def save(self, *args, **kwargs):
        new = not self.id
        # on fait un premier save pour avoir le fichier gpx présent
        super().save(*args, **kwargs)  # Call the "real" save() method.
        if new:
            valeurs = gpxcalc.ajout_route(self.gpx.path)
            self.nom = valeurs["nom"]
            self.lat = valeurs["lat"]
            self.lon = valeurs["lon"]
            self.distance = valeurs["distance"]
            self.positif = valeurs["positif"]
            self.negatif = valeurs["negatif"]
            self.geom = valeurs["geom"]
            self.altimax = valeurs["Altitude max"]
            self.altimin = valeurs["Altitude min"]
            self.estimationtemps = datetime.timedelta(minutes=round(60 / 4 * (valeurs["distance"] / 1000 +
                                    (valeurs["positif"] / 125) +
                                    (valeurs["negatif"] / 400))
                                    ))
            super().save(*args, **kwargs)  # Call the "real" save() method again.


@receiver(post_save, sender=Route)
def set_values(sender, instance, created, **kwargs):
    """"
    Ajout du tdm
    """
    if created:
        tdm_list = gpxcalc.get_tdm(instance.gpx.path)
        for i, etape in enumerate(tdm_list):
            tdm = Tdm()
            tdm.etape = etape["Étape"]
            tdm.route_id = instance
            tdm.rang = i + 1
            tdm.utm = etape["UTM"]
            tdm.azimut = etape["Azimut"]
            tdm.positif = etape["Dénivelé positif"]
            tdm.negatif = etape["Dénivelé négatif"]
            tdm.distance = etape["Distance"]
            tdm.cumul_distance = etape["Cumul distance"]
            tdm.temps_segment = etape["Temps segment"]
            tdm.temps_cumul = etape["Temps cumulé"]
            tdm.pause = etape["Pause"]
            tdm.geom = etape["geom"]
            tdm.save()


@receiver(pre_delete, sender=Route)
def remove_file(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.gpx:
        if os.path.isfile(instance.gpx.path):
            os.remove(instance.gpx.path)


class RouteFilter(django_filters.FilterSet):
    class Meta:
        model = Route
        fields = {"nom": ['contains'],
                  "distance": ['lt', 'gt'],
                  "positif": ['lt', 'gt'],
                  "estimationtemps": ['lt'],
        }


class Tdm(models.Model):
    """Cet objet représente un tableau de marche."""
    route_id = models.ForeignKey(Route, on_delete=models.CASCADE)
    rang = models.PositiveIntegerField(editable=False)
    etape = models.CharField(max_length=50)
    utm = models.CharField(max_length=20, editable=False)
    azimut = models.CharField(max_length=3, editable=False)
    positif = models.PositiveIntegerField(verbose_name="Dénivelé positif", editable=False)
    negatif = models.PositiveIntegerField(verbose_name="Dénivelé négatif", editable=False)
    distance = models.PositiveIntegerField(verbose_name="Distance", editable=False)
    cumul_distance = models.PositiveIntegerField(verbose_name="Cumul distance", editable=False)
    temps_segment = models.CharField(verbose_name="Temps", max_length=5)
    temps_cumul = models.CharField(verbose_name="Cumul temps", max_length=5)
    pause = models.CharField(max_length=5)
    note = models.TextField(blank=True)
    geom = models.TextField(default=None, blank=True, null=True, editable=False)

    class Meta:
        constraints = [models.UniqueConstraint(fields=["route_id", "rang"], name="route_rang")]

    def __str__(self):
        """Fonction requise par Django pour manipuler les objets Tdm dans la base de données."""
        return f'{self.route_id}, {self.rang} {self.etape}'

    def get_absolute_url(self):
        """Cette fonction est requise par Django, lorsque vous souhaitez détailler le contenu d'un objet."""
        return reverse('tdm-detail', args=[str(self.id)])
