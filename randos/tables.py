import django_tables2 as tables

from django_tables2.export.views import ExportMixin

from randos.models import Route, Tdm


class RoundColumn(tables.Column):
    def render(self, value):
        return round(value)


class RouteTable(tables.Table):
    nom = tables.Column(linkify=True)
    altimin = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    altimax = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    distance = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    positif = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    negatif = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    estimationtemps = tables.Column(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    cotation = tables.Column(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })

    class Meta:
        model = Route
        template_name = "django_tables2/bootstrap.html"
        fields = ("nom", "altimin", "altimax", "distance", "positif", "negatif",
                  "estimationtemps", "cotation")
        localize = ("altimin", "altimin", "distance", "positif", "negatif")


class TdmTable(ExportMixin, tables.Table):
    utm = tables.Column(attrs={
        "td": {"align": "right"}
    })
    azimut = tables.Column(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    positif = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    negatif = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    distance = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    cumul_distance = RoundColumn(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    temps_segment = tables.Column(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    temps_cumul = tables.Column(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })
    pause = tables.Column(attrs={
        "td": {"align": "right",
               "style": "width:1%;"}
    })

    class Meta:
        model = Tdm
        template_name = "django_tables2/bootstrap.html"
        fields = ("etape", "utm", "azimut", "positif", "negatif", "distance", "cumul_distance",
                  "temps_segment", "temps_cumul", "pause", "note")
        localize = ("positif", "negatif", "distance", "cumul_distance")
        orderable = False
