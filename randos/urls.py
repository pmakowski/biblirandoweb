from django.urls import path

from randos.views import RouteListView, route_detail_view, route_carte

urlpatterns = [
    path('', RouteListView.as_view()),
    path('<int:pk>/', route_detail_view, name='route-detail'),
    path('carte/<int:pk>/', route_carte, name='route-carte'),
]
