from django.contrib import admin

from randos.models import Route, Tdm

admin.site.register(Route)
admin.site.register(Tdm)
