from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin
from django_tables2.export.export import TableExport
from django.shortcuts import render, get_object_or_404

from randos.models import Route, Tdm, RouteFilter
from randos.tables import RouteTable, TdmTable
from . import gpxcalc


class RouteListView(SingleTableMixin, FilterView):
    model = Route
    table_class = RouteTable
    template_name = 'index.html'
    context_object_name = 'route_list'
    filterset_class = RouteFilter


def route_detail_view(request, pk, template_name='route.html'):
    route_detail = get_object_or_404(Route, pk=pk)
    tdm = Tdm.objects.filter(route_id=route_detail).order_by('rang')
    table = TdmTable(tdm)
    export_format = request.GET.get("_export", None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response("table.{}".format(export_format))
    ctx = {
        'route_detail': route_detail,
        'tdm': tdm,
        'table': table
    }
    return render(request, template_name, ctx)


def route_carte(request, pk, template_name='carte.html'):
    route_detail = get_object_or_404(Route, pk=pk)
    tdm = Tdm.objects.filter(route_id=route_detail).order_by('rang')
    table = TdmTable(tdm)
    export_format = request.GET.get("_export", None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response("table.{}".format(export_format))
    portrait = gpxcalc.get_orientation(route_detail.gpx.path)
    ctx = {
        'route_detail': route_detail,
        'tdm': tdm,
        'table': table,
        'portrait': portrait
    }
    return render(request, template_name, ctx)
