import datetime
import math

import gpxpy
import utm

PLAT = 5000
ASC = 400
DESC = 500
PAUSE = 30


def get_haversine(lat1, lng1, lat2, lng2):
    """formule de haversine en mètres"""
    _AVG_EARTH_RADIUS_M = 6371008.8
    lat1 = math.radians(lat1)
    lng1 = math.radians(lng1)
    lat2 = math.radians(lat2)
    lng2 = math.radians(lng2)
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = math.sin(lat * 0.5) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(lng * 0.5) ** 2
    return 2 * _AVG_EARTH_RADIUS_M * math.asin(math.sqrt(d))


def get_azimuth(lat1, lon1, lat2, lon2):
    """Calcul azimuth"""
    dlon = math.radians(lon2 - lon1)
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    x = math.cos(lat2) * math.sin(dlon)
    y = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(dlon)
    azimut = round(((math.atan2(x, y) * 180 / math.pi) + 360) % 360)
    return azimut


def get_elevation_distance(gpx):
    ele_list = []
    distance_list = []
    total_distance = 0
    first = True
    for route in gpx.routes:
        for point in route.points:
            if first:
                first = False
                distance = 0
                ele_list.append(int(point.elevation))
                total_distance = 0 + distance
                distance_list.append(total_distance)
                lat_ref = float(point.latitude)
                lon_ref = float(point.longitude)
            else:
                lat = float(point.latitude)
                lon = float(point.longitude)
                partiel = get_haversine(lat_ref, lon_ref, lat, lon)
                ele_list.append(int(point.elevation))
                total_distance = total_distance + partiel
                distance_list.append(total_distance)
                lat_ref = lat
                lon_ref = lon
    return distance_list, ele_list


def get_distance_denivele(gpx):
    total_distance = 0
    positif = 0
    negatif = 0
    first = True
    for route in gpx.routes:
        for point in route.points:
            if first:
                first = False
                distance = 0
                total_distance = 0 + distance
                lat_ref = float(point.latitude)
                lon_ref = float(point.longitude)
                ele_ref = float(point.elevation)
            else:
                lat = float(point.latitude)
                lon = float(point.longitude)
                ele = float(point.elevation)
                if (ele - ele_ref) > 0:
                    positif = positif + (ele - ele_ref)
                else:
                    negatif = negatif + (ele_ref - ele)
                partiel = get_haversine(lat_ref, lon_ref, lat, lon)
                total_distance = total_distance + partiel
                lat_ref = lat
                lon_ref = lon
                ele_ref = ele
    return total_distance, positif, negatif


def trk2rte(gpx, gpxfilepath):
    gpx_out = gpxpy.gpx.GPX()
    gpx_rte = gpxpy.gpx.GPXRoute(gpx.tracks[0].name)
    gpx_out.routes.append(gpx_rte)
    for point in gpx.walk(only_points=True):
        point.elevation = round(point.elevation)
        gpx_rte.points.append(point)
    with open(gpxfilepath, "w") as f:
        f.write(gpx_out.to_xml())


def ajout_route(gpxfilepath):
    with open(gpxfilepath, "r") as gpx_xml:
        gpx = gpxpy.parse(gpx_xml.read())
    # S'il n'y a pas de route, on la genère à partir le la trace
    if not gpx.routes:
        trk2rte(gpx, gpxfilepath)
        with open(gpxfilepath, "r") as gpx_xml:
            gpx = gpxpy.parse(gpx_xml.read())
    distance, positif, negatif = get_distance_denivele(gpx)
    point_lst = []
    altimax = 0
    altimin = 99999
    for route in gpx.routes:
        nom = route.name
        lat = route.points[0].latitude
        lon = route.points[0].longitude
        for point in route.points:
            point_lst.append([point.latitude, point.longitude])
            altimin = min(altimin, point.elevation)
            altimax = max(altimax, point.elevation)
    dict_return = dict()
    dict_return["nom"] = nom
    dict_return["lat"] = lat
    dict_return["lon"] = lon
    dict_return["distance"] = distance
    dict_return["positif"] = positif
    dict_return["negatif"] = negatif
    dict_return["geom"] = str(point_lst)
    dict_return["Altitude max"] = altimax
    dict_return["Altitude min"] = altimin
    return dict_return


def get_tdm(gpxfilepath, plat=PLAT, asc=ASC, desc=DESC, pause=PAUSE, km_effort=False):
    with open(gpxfilepath, "r") as gpx_xml:
        gpx = gpxpy.parse(gpx_xml.read())
    first = True
    tdm_dict = dict()
    tdm_list = []
    for route in gpx.routes:
        for point in route.points:
            if first:
                first = False
                utm_val = utm.from_latlon(point.latitude, point.longitude)
                distance = 0
                cumul_distance = 0
                positif = 0
                negatif = 0
                lat_ref = float(point.latitude)
                lon_ref = float(point.longitude)
                ele_ref = int(point.elevation)
                azimut = None
                temps_segment = 0
                cumul_temps = 0
                tdm_dict["Étape"] = "Départ"
                tdm_dict[
                    "UTM"
                ] = f"{utm_val[2]}{utm_val[3]} {int(utm_val[0]):07d} {int(utm_val[1]):07d}"
                tdm_dict["Azimut"] = ""
                tdm_dict["Altitude"] = ele_ref
                tdm_dict["Dénivelé positif"] = positif
                tdm_dict["Dénivelé négatif"] = negatif
                tdm_dict["Distance"] = 0
                tdm_dict["Cumul distance"] = 0
                tdm_dict["Temps segment"] = ""
                tdm_dict["Temps cumulé"] = ""
                tdm_dict["Pause"] = ""
                tdm_dict["geom"] = f"[{float(point.latitude)}, {float(point.longitude)}]"
                tdm_list.append(tdm_dict)
            else:
                lat = float(point.latitude)
                lon = float(point.longitude)
                partiel = get_haversine(lat_ref, lon_ref, lat, lon)
                distance = distance + partiel
                if not azimut and lat != lat_ref and lon != lon_ref:
                    azimut = round(get_azimuth(lat_ref, lon_ref, lat, lon))
                ele = point.elevation
                if (ele - ele_ref) > 0:
                    positif = positif + (ele - ele_ref)
                else:
                    negatif = negatif + (ele_ref - ele)
                try:
                    pente = ((ele - ele_ref) / partiel) * 100
                except ZeroDivisionError:
                    pente = 0
                if pente > 10:
                    temps = 60 / asc * (ele - ele_ref)
                elif pente < -10:
                    temps = 60 / desc * (ele_ref - ele)
                else:
                    temps = 60 / plat * partiel
                if km_effort:
                    temps = (
                            60 / plat * (partiel / 1000 + (positif / 125) + (negatif / 400) * 1000)
                    )
                temps_segment = temps_segment + temps
                tdm_dict = dict()
                for waypoint in gpx.waypoints:
                    if (
                            waypoint.latitude == point.latitude
                            and waypoint.longitude == point.longitude
                            and waypoint.elevation == point.elevation
                            and not (waypoint.latitude == ele_ref
                                     and waypoint.longitude == lon_ref
                                     and waypoint.latitude == lat_ref)
                    ):
                        tdm_dict["Étape"] = waypoint.name
                    elif point == route.points[-1]:
                        tdm_dict["Étape"] = "Arrivée"
                lat_ref = lat
                lon_ref = lon
                ele_ref = ele
                if tdm_dict:
                    utm_val = utm.from_latlon(lat, lon)
                    tdm_dict["geom"] = f"[{lat}, {lon}]"
                    tdm_dict[
                        "UTM"
                    ] = f"{utm_val[2]}{utm_val[3]} {int(utm_val[0]):07d} {int(utm_val[1]):07d}"
                    tdm_dict["Azimut"] = azimut
                    tdm_dict["Altitude"] = ele
                    tdm_dict["Dénivelé positif"] = positif
                    tdm_dict["Dénivelé négatif"] = negatif
                    tdm_dict["Distance"] = round(distance)
                    cumul_distance = cumul_distance + distance
                    tdm_dict["Cumul distance"] = round(cumul_distance)
                    tdm_dict["Temps segment"] = str(
                        datetime.timedelta(minutes=round(temps_segment))
                    )[:-3]
                    cumul_temps = cumul_temps + temps_segment
                    tdm_dict["Temps cumulé"] = str(
                        datetime.timedelta(minutes=round(cumul_temps))
                    )[:-3]
                    tdm_dict["Pause"] = str(
                        datetime.timedelta(minutes=round(temps_segment * pause / 100))
                    )[:-3]
                    if tdm_dict["Azimut"]:
                        tdm_list.append(tdm_dict)
                    distance = 0
                    temps_segment = 0
                    positif = 0
                    negatif = 0
                    azimut = None
    if not gpx.waypoints:
        tdm_list = []
    return tdm_list


def get_orientation(gpxfilepath):
    with open(gpxfilepath, "r") as gpx_xml:
        gpx = gpxpy.parse(gpx_xml.read())
    min_lat = None
    max_lat = None
    min_lon = None
    max_lon = None
    for point in gpx.routes[0].walk(only_points=True):
        if min_lat is None or point.latitude < min_lat:
            min_lat = point.latitude
        if max_lat is None or point.latitude > max_lat:
            max_lat = point.latitude
        if min_lon is None or point.longitude < min_lon:
            min_lon = point.longitude
        if max_lon is None or point.longitude > max_lon:
            max_lon = point.longitude
    centre_lat = (max_lat + min_lat) / 2
    centre_lon = (max_lon + min_lon) / 2
    ecart_lat = get_haversine(max_lat, centre_lon, min_lat, centre_lon)
    ecart_lon = get_haversine(centre_lat, max_lon, centre_lat, min_lon)
    portrait = (ecart_lat > ecart_lon)
    return portrait
