from django.apps import AppConfig


class RandosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'randos'
