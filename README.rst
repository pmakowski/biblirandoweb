|License|

*************
BibliRandoWeb
*************

Gestion d'une bibliothèque de randonnées.


Utilisation
===========

C'est une application Django, il faut donc suivre les indications de la documentation de Django.

Les réglages locaux sont à mettre dans un fichier settings_local.py dans le même répertoire que settings.py.

Il y a certainement pleins d'améliorations possibles, mais pour l'instant, cela correspond à mes besoins.


Dépendances
===========

Les bibliothèques suivantes sont utilisées :

- `gpxpy <https://pypi.org/project/gpxpy/>`_
- `utm <https://pypi.org/project/utm/>`_
- `Django <https://www.djangoproject.com//>`_
- `django-bootstrap4 <https://pypi.org/project/django-bootstrap4/>`_
- `django-filter <https://pypi.org/project/django-filter/>`_
- `SQLite <https://sqlite.org/>`_
- `django-leaflet <https://pypi.org/project/django-leaflet/>`_
- `django-tables2 <https://pypi.org/project/django-tables2/>`_
- `tablib <https://pypi.org/project/tablib/>`_


Les cartes sont celles d' `OpenTopoMap <https://opentopomap.org>`_


Auteur
======

BibliRando a été créé par `Philippe Makowski <https://gitlab.com/pmakowski>`_

Merci aussi pour l'inspiration à deux projets :

- `FitTrackee <https://github.com/SamR1/FitTrackee>`_
- `gpxRando <https://github.com/delord13/gpxRando>`_


License
=======

GNU Affero General Public License v3.0 or later

Voir `COPYING <COPYING>`_ pour lire le texte en entier.

.. |License| image:: https://img.shields.io/badge/license-AGPL%20v3.0-brightgreen.svg
   :target: COPYING
   :alt: Repository License

